const path = require('path');
const fs = require('fs');
const parse = require('csv-parse/lib/sync');
const tf = require('@tensorflow/tfjs-node');

const DATA_REVOLVER = path.resolve('data', 'revolver');

const training = [];
const output = [];
const test = [];


const on_record = (record, context) => {
    return record.slice(1);
};

let files = fs.readdirSync(path.resolve(DATA_REVOLVER, 'true'));
files.forEach(f => {
    const data = fs.readFileSync(path.resolve(DATA_REVOLVER, 'true', f));
    let results = tf.tensor2d(parse(data, {on_record: on_record, cast: true}));
    training.push(results.as1D().arraySync());
    output.push([1]);
});

files = fs.readdirSync(path.resolve(DATA_REVOLVER, 'false'));
files.forEach(f => {
    const data = fs.readFileSync(path.resolve(DATA_REVOLVER, 'false', f));
    let results = tf.tensor2d(parse(data, {on_record: on_record, cast: true}));
    training.push(results.as1D().arraySync());
    output.push([0]);
});

files = fs.readdirSync(path.resolve(DATA_REVOLVER, 'test'));
files.forEach(f => {
    const data = fs.readFileSync(path.resolve(DATA_REVOLVER, 'test', f));
    let results = tf.tensor2d(parse(data, {on_record: on_record, cast: true}));
    test.push(results.as1D().arraySync());
});

const model = tf.sequential();
   model.add(tf.layers.dense({ units: 128*5, activation: 'sigmoid', inputShape: [128*3] }));
   model.add(tf.layers.dense({ units: 3, activation: 'sigmoid', inputShape: [128*5]}));
   model.add(tf.layers.dense({ units: 1, activation: 'sigmoid' }));

model.compile({
    loss: 'meanSquaredError',
    optimizer: tf.train.adam(.06)
});;

model.fit(tf.tensor2d(training), tf.tensor2d(output), {epochs: 100}).then((history)=>{
    console.log('done');
    model.predict(tf.tensor2d(test)).print();
    model.save(`file://${path.resolve('models')}`);
});


